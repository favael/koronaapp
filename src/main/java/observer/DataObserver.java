package observer;

import model.GlobalData;

// Służy do odbierania informacji na temat nowy danych.
// Element wzorca projektowego "obserwator".
public interface DataObserver {
    void onData(GlobalData data);
}
