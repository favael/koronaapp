package model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


// Nasz model danych.

public class GlobalData {

 @JsonIgnoreProperties
 public long updated;
 public int cases;
 public int todayCases;
 public long deaths;
 public long todayDeaths;
 public int recovered;
 public int active;
 public int critical;
 public int casesPerOneMillion;
 public int deathsPerOneMillion;
 public int tests;
 public int testsPerOneMillion;
 public int affectedCountries;




}
