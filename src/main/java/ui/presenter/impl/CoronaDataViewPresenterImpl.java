package ui.presenter.impl;

import model.GlobalData;
import observer.DataObserver;
import reader.DataReader;
import ui.presenter.CoronaDataViewPresenter;
import ui.view.CoronaDataView;

public class CoronaDataViewPresenterImpl implements
        CoronaDataViewPresenter,
        DataObserver
{
    private CoronaDataView view;
    private DataReader reader;

    public CoronaDataViewPresenterImpl(DataReader reader) {
        if (reader == null) {
            throw new RuntimeException("DataReader cannot be null!");
        }
        this.reader = reader;
    }

    // Prezenter przyjmuje widok, następnie wyświetla go, rejestruje się jako obserwator
    // reader-a i zleca odczyt danych. Prezenter zawiera logikę.
    @Override
    public void setView(CoronaDataView view) {
        if (view == null) {
            return;
        }
        this.view = view;
        view.show();
        reader.addObserver(this);
        reader.requestData();
    }

    // Metoda obserwatora. Prezenter otrzymuje w niej nowe dane od reader-a
    // i przekazuje je do widoku.
    @Override
    public void onData(GlobalData data) {
        if (data == null) {
            return;
        }
        String updatedStr = String.valueOf(data.updated);
        String casesStr = String.valueOf(data.cases);
        String todayCasesStr = String.valueOf(data.todayCases);
        String deathsStr = String.valueOf(data.deaths);
        String todayDeathsStr = String.valueOf(data.todayDeaths);
        String recoveredStr= String.valueOf(data.recovered);
        String activeStr = String.valueOf(data.active);
        String criticalStr = String.valueOf(data.critical);
        String casesPerOneMillionStr = String.valueOf(data.casesPerOneMillion);
        String deathPerMillionStr = String.valueOf(data.deathsPerOneMillion);
        String testsStr = String.valueOf(data.tests);
        String testsPerOneMillionStr = String.valueOf(data.testsPerOneMillion);
        String affectedStr = String.valueOf(data.affectedCountries);
        view.updateUpdated(updatedStr);
        view.updateCases(casesStr);
        view.updateTodayCases(todayCasesStr);
        view.updateDeaths(deathsStr);
        view.updateTodayDeaths(todayDeathsStr);
        view.updateRecovered(recoveredStr);
        view.updateActive(activeStr);
        view.updateCritical(criticalStr);
        view.updateCasesPerOneMillion(casesPerOneMillionStr);
        view.updateDeathsPerOneMillion(deathPerMillionStr);
        view.updateTests(testsStr);
        view.updateTestsPerOneMillion(testsPerOneMillionStr);
        view.updateAffectedCountries(affectedStr);



    }
}
