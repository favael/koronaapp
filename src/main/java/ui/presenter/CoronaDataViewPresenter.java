package ui.presenter;

import ui.view.CoronaDataView;

// Uogólnienie dla prezentera widoku CoronaDataView.
// Prezenter odpowiedzialny jest za pośredniczenie między widokiem a modelem danych,
// dzięki czemu nie musza one o sobie wiedzieć.
public interface CoronaDataViewPresenter {
    void setView(CoronaDataView view);
}
