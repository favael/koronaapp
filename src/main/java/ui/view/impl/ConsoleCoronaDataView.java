package ui.view.impl;

import ui.view.CoronaDataView;

// Implementacja widoku CoronaDataView wykorzystującą konsolę.
public class ConsoleCoronaDataView implements CoronaDataView {

    @Override
    public void updateUpdated (String update) {System.out.println("Update: " + update); }
    @Override
    public void updateCases(String cases) { System.out.println("Global cases: " + cases); }

    @Override
    public void updateTodayCases (String todayCases) { System.out.println("TodayCases: " + todayCases); }

    @Override
    public void updateDeaths(String deaths) { System.out.println("Global deaths: " + deaths); }

    @Override
    public void updateTodayDeaths (String todayDeaths) { System.out.println("TodayDeaths: " + todayDeaths); }

    @Override
    public void updateRecovered(String recovered) {
        System.out.println("Global recovered: " + recovered);
    }

    @Override
    public void updateActive (String active) { System.out.println("Active: " + active); }

    @Override
    public void updateCritical (String critical) { System.out.println("Critical: " + critical); }

    @Override
    public void updateCasesPerOneMillion (String casesPerOneMillion) { System.out.println("Cases per one Million: " + casesPerOneMillion); }

    @Override
    public void updateDeathsPerOneMillion (String deathsPerOneMilion) { System.out.println("Deaths per one Million: " + deathsPerOneMilion); }

    @Override
    public void updateTests (String tests) { System.out.println("Test: " + tests); }

    @Override
    public void updateTestsPerOneMillion (String testPerOneMillion) {System.out.println("Test per one million:" + testPerOneMillion); }

    @Override
    public void updateAffectedCountries (String affectedCountries) {System.out.println("Affected countries: " + affectedCountries); }


    @Override
    public void show() {

    }
}
