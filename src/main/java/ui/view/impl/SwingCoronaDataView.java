package ui.view.impl;

import ui.view.CoronaDataView;

import javax.swing.*;

// Implementacja widoku CoronaDataView wykorzystującą bibliotekę Swing.
public class SwingCoronaDataView implements CoronaDataView {
    private JFrame window;

    private JPanel updatedPanel;
    private JPanel casesPanel;
    private JPanel todayCasesPanel;
    private JPanel deathsPanel;
    private JPanel todayDeathsPanel;
    private JPanel recoveredPanel;
    private JPanel activePanel;
    private JPanel criticalPanel;
    private JPanel casesPerOneMillionPanel;
    private JPanel deathsPerOneMillionPanel;
    private JPanel testsPanel;
    private JPanel testsPerOneMillionPanel;
    private JPanel affectedCountriesPanel;

    private JTextField updatedLabel;
    private JTextField casesLabel;
    private JTextField todayCasesLabel;
    private JTextField deathsLabel;
    private JTextField todayDeathsLabel;
    private JTextField recoveredLabel;
    private JTextField activeLabel;
    private JTextField criticalLabel;
    private JTextField casesPerOneMillionLabel;
    private JTextField deathsPerOneMillionLabel;
    private JTextField testsLabel;
    private JTextField testsPerOneMillionLabel;
    private JTextField affectedCountriesLabel;
    private JTextField updated;
    private JTextField cases;
    private JTextField todayCases;
    private JTextField deaths;
    private JTextField todayDeaths;
    private JTextField recovered;
    private JTextField active;
    private JTextField critical;
    private JTextField casesPerOneMillion;
    private JTextField deathsPerOneMillion;
    private JTextField tests;
    private JTextField testsPerOneMillion;
    private JTextField affectedCountries;

    public SwingCoronaDataView() {
        // Tworzy okienko Swing
        window = new JFrame("COVID-19 Tracker");

        window.setLayout(new BoxLayout(window.getContentPane(), BoxLayout.PAGE_AXIS));
        // Tworzy pole tekstowe i panel

        updated = new JTextField();
        updatedLabel = new JTextField("Updated:");
        updatedPanel = new JPanel();
        updatedPanel.add(updatedLabel);
        updatedPanel.add(updated);
        updatedPanel.setLayout(new BoxLayout(updatedPanel, BoxLayout.LINE_AXIS));

        cases = new JTextField();
        casesLabel = new JTextField("Cases:");
        casesPanel = new JPanel();
//         Dodaje pola tekstowe do panelu
        casesPanel.add(casesLabel);
        casesPanel.add(cases);
//         Ustawia styl rozmieszczania elementów panelu na horyzontalny.
        casesPanel.setLayout(new BoxLayout(casesPanel, BoxLayout.LINE_AXIS));

        todayCases = new JTextField();
        todayCasesLabel = new JTextField("Today cases:");
        todayCasesPanel = new JPanel();
        todayCasesPanel.add(todayCasesLabel);
        todayCasesPanel.add(todayCases);
        todayCasesPanel.setLayout(new BoxLayout(todayCasesPanel, BoxLayout.LINE_AXIS));

        deaths = new JTextField();
        deathsLabel = new JTextField("Deaths:");
        deathsPanel = new JPanel();
        deathsPanel.add(deathsLabel);
        deathsPanel.add(deaths);
        deathsPanel.setLayout(new BoxLayout(deathsPanel, BoxLayout.LINE_AXIS));

        todayDeaths = new JTextField();
        todayDeathsLabel = new JTextField("Today deaths:");
        todayDeathsPanel = new JPanel();
        todayDeathsPanel.add(todayDeathsLabel);
        todayDeathsPanel.add(todayDeaths);
        todayDeathsPanel.setLayout(new BoxLayout(todayDeathsPanel, BoxLayout.LINE_AXIS));

        recovered = new JTextField();
        recoveredLabel = new JTextField("Recovered:");
        recoveredPanel = new JPanel();
        recoveredPanel.add(recoveredLabel);
        recoveredPanel.add(recovered);
        recoveredPanel.setLayout(new BoxLayout(recoveredPanel, BoxLayout.LINE_AXIS));

        active = new JTextField();
        activeLabel = new JTextField("Active:");
        activePanel = new JPanel();
        activePanel.add(activeLabel);
        activePanel.add(active);
        activePanel.setLayout(new BoxLayout(activePanel, BoxLayout.LINE_AXIS));

        critical = new JTextField();
        criticalLabel = new JTextField("Critical:");
        criticalPanel = new JPanel();
        criticalPanel.add(criticalLabel);
        criticalPanel.add(critical);
        criticalPanel.setLayout(new BoxLayout(criticalPanel, BoxLayout.LINE_AXIS));

        casesPerOneMillion = new JTextField();
        casesPerOneMillionLabel = new JTextField("Cases per one million:");
        casesPerOneMillionPanel = new JPanel();
        casesPerOneMillionPanel.add(casesPerOneMillionLabel);
        casesPerOneMillionPanel.add(casesPerOneMillion);
        casesPerOneMillionPanel.setLayout(new BoxLayout(casesPerOneMillionPanel, BoxLayout.LINE_AXIS));

        deathsPerOneMillion = new JTextField();
        deathsPerOneMillionLabel = new JTextField("Death per one million:");
        deathsPerOneMillionPanel = new JPanel();
        deathsPerOneMillionPanel.add(deathsPerOneMillionLabel);
        deathsPerOneMillionPanel.add(deathsPerOneMillion);
        deathsPerOneMillionPanel.setLayout(new BoxLayout(deathsPerOneMillionPanel, BoxLayout.LINE_AXIS));

        tests = new JTextField();
        testsLabel = new JTextField("Tests:");
        testsPanel = new JPanel();
        testsPanel.add(testsLabel);
        testsPanel.add(tests);
        testsPanel.setLayout(new BoxLayout(testsPanel, BoxLayout.LINE_AXIS));


        testsPerOneMillion = new JTextField();
        testsPerOneMillionLabel = new JTextField("Test per one million:");
        testsPerOneMillionPanel = new JPanel();
        testsPerOneMillionPanel.add(testsPerOneMillionLabel);
        testsPerOneMillionPanel.add(testsPerOneMillion);
        testsPerOneMillionPanel.setLayout(new BoxLayout(testsPerOneMillionPanel, BoxLayout.LINE_AXIS));


        affectedCountries = new JTextField();
        affectedCountriesLabel = new JTextField("Affected countries:");
        affectedCountriesPanel = new JPanel();
        affectedCountriesPanel.add(affectedCountriesLabel);
        affectedCountriesPanel.add(affectedCountries);
        affectedCountriesPanel.setLayout(new BoxLayout(affectedCountriesPanel, BoxLayout.LINE_AXIS));


        window.add(updatedPanel);
        window.add(casesPanel);
        window.add(todayCasesPanel);
        window.add(deathsPanel);
        window.add(todayDeathsPanel);
        window.add(recoveredPanel);
        window.add(activePanel);
        window.add(criticalPanel);
        window.add(casesPerOneMillionPanel);
        window.add(deathsPerOneMillionPanel);
        window.add(testsPanel);
        window.add(testsPerOneMillionPanel);
        window.add(affectedCountriesPanel);

        window.setSize(750, 120);
        window.setLocation(100, 100);
    }
    @Override
    public void updateUpdated (String updateStr) { this.updated.setText(updateStr); }

    @Override
    public void updateCases(String cases) { this.cases.setText(cases); }

    @Override
    public void updateTodayCases (String todayCasesStr) {this.todayCases.setText(todayCasesStr); }

    @Override
    public void updateDeaths(String deaths) { this.deaths.setText(deaths); }

    @Override
    public void updateTodayDeaths (String todayDeaths) {this.todayDeaths.setText(todayDeaths); }

    @Override
    public void updateRecovered(String recovered) { this.recovered.setText(recovered); }

    @Override
    public void updateActive (String activeStr) {this.active.setText(activeStr); }

    @Override
    public void updateCritical (String criticalStr) {this.critical.setText(criticalStr); }

    @Override
    public void updateCasesPerOneMillion (String casesPerOneMillionStr) {this.casesPerOneMillion.setText(casesPerOneMillionStr); }

    @Override
    public void updateDeathsPerOneMillion (String deathsPerOneMilion) {this.deathsPerOneMillion.setText(deathsPerOneMilion); }

    @Override
    public void updateTests (String testsStr) {this.tests.setText(testsStr); }

    @Override
    public void updateTestsPerOneMillion (String testPerOneMillionStr) {this.testsPerOneMillion.setText(testPerOneMillionStr); }

    @Override
    public void updateAffectedCountries (String affectedCountries) {this.affectedCountries.setText(affectedCountries); }

    @Override
    public void show() { window.setVisible(true); }
}