package ui.view;

// Uogólnienie widoku danych na temat pandemii.
// Widok wyświetla informacje nt. liczby przypadków (cases), liczby zgonów (deaths)
// oraz liczby wyzdrowiałych (recovered).
public interface CoronaDataView {
    void updateUpdated(String update);
    void updateCases(String cases);
    void updateTodayCases(String todayCases);
    void updateDeaths(String deaths);
    void updateTodayDeaths(String todayDeaths);
    void updateRecovered(String recovered);
    void updateActive(String active);
    void updateCritical(String critical);
    void updateCasesPerOneMillion(String casesPerOneMillion);
    void updateDeathsPerOneMillion(String deathsPerOneMilion);
    void updateTests(String tests);
    void updateTestsPerOneMillion(String testPerOneMillion);
    void updateAffectedCountries(String affectedCountries);
    void show();
}
